import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BannerComponent } from './components/banner/banner.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { BodyComponent } from './components/body/body.component';
import { FooterComponent } from './components/footer/footer.component';
import { ListUserComponent } from './components/body/user/list-user/list-user.component';
import { ListPrestaComponent } from './components/body/presta/list-presta/list-presta.component';

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    NavBarComponent,
    BodyComponent,
    FooterComponent,
    ListUserComponent,
    ListPrestaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
